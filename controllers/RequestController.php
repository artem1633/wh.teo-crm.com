<?php

namespace app\controllers;

use app\models\Candidate;
use app\models\forms\CandidateForm;
use app\models\RequestFile;
use app\models\RequestFileSearch;
use app\models\Settings;
use app\models\User;
use app\modules\api\controllers\V1Controller;
use PHPHtmlParser\Dom;
use Yii;
use app\models\Request;
use app\models\RequestSearch;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * RequestController implements the CRUD actions for Request model.
 */
class RequestController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Request models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RequestSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        if(Yii::$app->user->identity->isSuperAdmin() == false && Yii::$app->user->identity->isCandidate() == false){
            $dataProvider->query->andWhere(['client_id' => Yii::$app->user->getId()])->andWhere(['not in', 'status', [Request::STATUS_CLOSED, Request::STATUS_FAILED]]);
        }

        if(Yii::$app->user->identity->isCandidate()){
            $dataProvider->query->andWhere(['not in', 'status', [Request::STATUS_DONE]]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Displays a single Request model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title'=> "request #".$id,
                'content'=>$this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Save',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }


    /**
     * @param $id
     * @return array|string
     * @throws NotFoundHttpException
     */
    public function actionViewFiles($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $filesSearchModel = new RequestFileSearch();
        $filesDataProvider = $filesSearchModel->search([]);
        $filesDataProvider->query->andWhere(['request_id' => $id]);
        $filesDataProvider->pagination = false;

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load($request->post());
            $model->save(false);
            return [
                'title'=> "request #".$id,
                'content'=>$this->renderAjax('view-files', [
                    'model' => $model,
                    'filesSearchModel' => $filesSearchModel,
                    'filesDataProvider' => $filesDataProvider,
                ]),
                'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }else{
            return $this->render('view-files', [
                'model' => $model,
                'filesSearchModel' => $filesSearchModel,
                'filesDataProvider' => $filesDataProvider,
            ]);
        }
    }

    /**
     * @param integer $id
     * @throws NotFoundHttpException
     */
    public function actionDownloadFile($id)
    {
        $tenderFile = RequestFile::findOne($id);

        if($tenderFile == null){
            throw new NotFoundHttpException();
        }

        if(file_exists($tenderFile->path)){
            Yii::$app->response->sendFile($tenderFile->path, $tenderFile->name);
        }
    }


    /**
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdateRecsPayment($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update recs payment #".$id,
                    'content'=>$this->renderAjax('update-recs-payment', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose' => true
                ];
            }else{
                return [
                    'title'=> "Update recs payment #".$id,
                    'content'=>$this->renderAjax('update-recs-payment', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing Request model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionApplyCandidate($id)
    {
        $request = Yii::$app->request;
        $requestModel = $this->findModel($id);
        $model = new CandidateForm(['requestId' => $id]);


        Yii::$app->response->format = Response::FORMAT_JSON;
        if($request->isGet){
            return [
                'title'=> "Apply candidate",
                'content'=>$this->renderAjax('apply_candidate', [
                    'model' => $model,
                    'request' => $requestModel,
                ]),
                'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }else if($model->load($request->post()) && $model->apply()){
            $request = $this->findModel($id);
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> "request #".$id,
                'content'=>$this->renderAjax('view', [
                    'model' => $request,
                ]),
                'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::a('Save',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
            ];
        }else{
            return [
                'title'=> "Apply candidate #".$id,
                'content'=>$this->renderAjax('apply_candidate', [
                    'model' => $model,
                    'request' => $requestModel,
                ]),
                'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
            ];
        }
    }

    public function actionReceive($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->status = Request::STATUS_DOCS_LOADED;
        $model->receive_date = date('Y-m-d');
        $model->save(false);

        return [
            'forceReload'=>'#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }

    public function actionToWork($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->status = Request::STATUS_WORK;
        $model->save(false);

        return [
            'forceReload'=>'#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionContinue($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $timeSetting = Settings::findByKey('second_timer');
        if($timeSetting == null){
            $timeSetting = 0;
        } else {
            $timeSetting = $timeSetting->value * 60;
        }
        $model->done_datetime = date('Y-m-d H:i:s', time() + $timeSetting); // 7200 — 2 hours
        $model->continued = 1;
        $model->save(false);

        return [
            'forceReload'=>'#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionFail($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        $model->status = Request::STATUS_FAILED;

        $model->save(false);

        return [
            'forceReload'=>'#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }

    /**
     * @param $id
     * @return bool|string
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionUploadFile($id)
    {
        $model = $this->findModel($id);
        $fileName = Yii::$app->security->generateRandomString();
        if(is_dir('uploads') == false){
            mkdir('uploads');
        }
        $uploadPath = 'uploads';

        if(isset($_FILES['file'])) {
            $file = UploadedFile::getInstanceByName('file');
            $path = $uploadPath.'/'.$fileName.'.'.$file->extension;

            $fileCount = RequestFile::find()->where(['request_id' => $id])->count();

            if($fileCount == 1){
//                $model->status = Request::STATUS_DOCS_LOADED;
                $adminIds = explode(',', Settings::findByKey('admin_telegram_ids')->value);
                foreach ($adminIds as $id) {
                    V1Controller::sendTelMessage($id, "New check loaded to request #{$id}");
                }
            }
            $model->save(false);
            if($file->saveAs($path)) {
                $file = new RequestFile([
                    'request_id' => $model->id,
                    'name' => $_FILES['file']['name'],
                    'path' => $path
                ]);
                $file->save(false);

                return Json::encode($file);
            }
        }

        return false;
    }

    /**
     * @param int $id
     * @return array
     */
    public function actionCheckBanks($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        $formModel = new CandidateForm();
        $formModel->requestId = $model->id;

        if($formModel->load($request->post()) && $formModel->apply()){
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> "Check banks",
                'content'=> '<span class="text-success">Bank apply</span>',
                'footer'=> Html::button('ОК',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"])

            ];
        }else{
            return [
                'title'=> "Check banks",
                'content'=> $this->renderAjax('check-banks', [
                    'model' => $model,
                    'formModel' => $formModel,
                ]),
                'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

            ];
        }
    }

    /**
     * @param int $id
     * @return array
     */
    public function actionNextCandidate($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        $model->nextVisibleCandidate();


        if ($model->candidate_visible_id != null) {
            $html = "New request created\n";
            foreach ($model->attributes as $key => $value) {
                $html .= "{$model->getAttributeLabel($key)}: {$model->$key}\n";
            }
            $candidate = Candidate::findOne($model->candidate_visible_id);
            if($candidate){
                $user = User::findOne($candidate->user_id);
                if($user){
                    if($user->telegram_id != null){
                        \Yii::warning($user->telegram_id);
                        V1Controller::sendTelMessage($user->telegram_id, $html);
                    }
                }
            }
        }


        return [
            'title'=> "Check banks",
            'forceReload'=>'#crud-datatable-pjax',
            'content'=> '<span class="text-danger">Sorry. No one bank is available</span>',
            'footer'=> Html::button('OK',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"]),
        ];
    }

    /**
     * @param string $zip
     * @param string $bank
     * @return array
     */
    public function actionCheckBank($zip, $bank)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $bank = str_replace(' ', '+', $bank);
        $html = file_get_contents("https://www.usbanklocations.com/banks.php?q={$bank}&ml=30&lc={$zip}");
        $dom = new Dom();
        $dom->load($html);

        $plb = $dom->find('.plb');

        if(count($plb) > 0){
            return ['result' => true];
        }

        return ['result' => false];
    }

    /**
     * Creates a new Request model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Request();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Add request",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Add request",
                    'content'=>'<span class="text-success">Request has been created</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::a('Create more',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])

                ];
            }else{
                return [
                    'title'=> "Add request",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionDone($id)
    {
        $request = Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);

        if($request->isGet){
            return [
                'title'=> "Done request",
                'content'=>$this->renderAjax('done', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

            ];
        }else if($model->load($request->post()) && $model->validate()){
            $model->status = Request::STATUS_DONE;
            $model->save(false);
            return [
                'forceReload'=>'#crud-datatable-pjax',
                'title'=> "Add request",
                'content'=>'<span class="text-success">Request is done</span>',
                'footer'=> Html::button('ОК',['class'=>'btn btn-default btn-block','data-dismiss'=>"modal"]),
            ];
        }else{
            return [
                'title'=> "Done request",
                'content'=>$this->renderAjax('done', [
                    'model' => $model,
                ]),
                'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                    Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])

            ];
        }
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionClose($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->status = Request::STATUS_CLOSED;

        $model->save(false);

        return [
            'forceReload'=>'#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionPayment($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $model = $this->findModel($id);
        $model->status = Request::STATUS_PAYMENT;

        $hours24 = time() + 86400; // 86400 — Сутки
        $model->done_datetime = date('Y-m-d H:i:s', $hours24);

        $model->save(false);

        return [
            'forceReload'=>'#crud-datatable-pjax',
            'forceClose' => true,
        ];
    }

    /**
     * Updates an existing Request model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update request #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose' => true,
                ];
            }else{
                return [
                    'title'=> "Update request #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                        Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Request model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Request model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Request model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Request the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Request::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
