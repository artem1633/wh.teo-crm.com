<?php

namespace app\controllers;

use Yii;
use app\models\forms\ReportForm;
use yii\web\Controller;

/**
 * Class ReportController
 * @package app\controllers
 */
class ReportController extends Controller
{
    /**
     *
     */
    public function actionIndex()
    {
        $model = new ReportForm();
        $clientDataProvider = $model->searchClient(Yii::$app->request->get());
        $candidateDataProvider = $model->searchCandidate(Yii::$app->request->get());

        return $this->render('index', [
            'model' => $model,
            'clientDataProvider' => $clientDataProvider,
            'candidateDataProvider' => $candidateDataProvider,
        ]);
    }
}