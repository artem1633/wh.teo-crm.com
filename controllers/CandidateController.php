<?php

namespace app\controllers;

use app\models\CandidateFile;
use app\models\CandidateFileSearch;
use app\models\Request;
use app\models\RequestSearch;
use kartik\grid\EditableColumnAction;
use Yii;
use app\models\Candidate;
use app\models\CandidateSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;
use yii\web\UploadedFile;

/**
 * CandidateController implements the CRUD actions for Candidate model.
 */
class CandidateController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'edit' => [
                'class' => EditableColumnAction::className(),
                'modelClass' => Candidate::className(),
                'showModelErrors' => true,
                'outputValue' => function($model, $attribute, $key, $index) {
                    if($attribute == 'status'){
                        return Candidate::statusLabels()[$model->$attribute];
                    }

                    return $model->$attribute;
                },
                'errorOptions' => ['header' => ''],
            ],
        ]);
    }

    /**
     * Lists all Candidate models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new CandidateSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * @param integer $candidate_id
     * @return bool|string
     */
    public function actionUploadFile($candidate_id)
    {
        $fileName = Yii::$app->security->generateRandomString();
        if(is_dir('uploads') == false){
            mkdir('uploads');
        }
        $uploadPath = 'uploads';

        if(isset($_FILES['file'])) {
            $file = UploadedFile::getInstanceByName('file');
            $path = $uploadPath.'/'.$fileName.'.'.$file->extension;

            if($file->saveAs($path)) {
                $tenderFile = new CandidateFile([
                    'candidate_id' => $candidate_id,
                    'name' => $_FILES['file']['name'],
                    'path' => $path
                ]);
                $tenderFile->save(false);

                return Json::encode($file);
            }
        }

        return false;
    }

    /**
     * @param integer $id
     * @throws NotFoundHttpException
     */
    public function actionDownloadFile($id)
    {
        $tenderFile = CandidateFile::findOne($id);

        if($tenderFile == null){
            throw new NotFoundHttpException();
        }

        if(file_exists($tenderFile->path)){
            Yii::$app->response->sendFile($tenderFile->path, $tenderFile->name);
        }
    }


    /**
     * Displays a single Candidate model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        $filesSearchModel = new CandidateFileSearch();
        $filesDataProvider = $filesSearchModel->search([]);
        $filesDataProvider->query->andWhere(['candidate_id' => $id]);
        $filesDataProvider->pagination = false;

        $requestSearchModel = new RequestSearch();
        $requestSearchModel->statuses = [Request::STATUS_CLOSED];
        $requestDataProvider = $requestSearchModel->search(Yii::$app->request->queryParams);
        $requestDataProvider->query->andWhere(['candidate_id' => $id]);

        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                    'title'=> "candidate #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $this->findModel($id),
                        'filesSearchModel' => $filesSearchModel,
                        'filesDataProvider' => $filesDataProvider,
                        'requestSearchModel' => $requestSearchModel,
                        'requestDataProvider' => $requestDataProvider,
                    ]),
                    'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Save',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
                'filesSearchModel' => $filesSearchModel,
                'filesDataProvider' => $filesDataProvider,
                'requestSearchModel' => $requestSearchModel,
                'requestDataProvider' => $requestDataProvider,
            ]);
        }
    }

    /**
     * Creates a new Candidate model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Candidate();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create candidate",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create candidate",
                    'content'=>'<span class="text-success">Candidate has been created</span>',
                    'footer'=> Html::button('ОК',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create more',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create candidate",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Candidate model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update candidate #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Done',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'forceClose' => true,
                ];
            }else{
                 return [
                    'title'=> "Update candidate #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Cancel',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Done',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Candidate model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Candidate model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Candidate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Candidate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Candidate::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Запрашиваемой страницы не существует.');
        }
    }
}
