<?php

namespace app\modules\api\controllers;

use app\models\Candidate;
use app\models\Settings;
use yii\rest\ActiveController;
use yii\web\Response;

/**
 * Default controller for the `api` module
 */
class V1Controller extends ActiveController
{
    public $modelClass = 'app\models\Api';

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'only' => ['test-tel'],
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionUpdateCandidateStatus()
    {
        Candidate::updateAll(['status' => Candidate::STATUS_AWAIT]);
    }


    public function actionTestTel()
    {
        $result = self::sendTelMessage('247187885', 'Hello from test method');

        return $result;
    }

    public function actionDailyTelMessage()
    {
        /** @var Candidate[] $candidates */
        $candidates = Candidate::find()->where(['is not', 'telegram_id', null])->all();
        $timeSetting = Settings::findByKey('daily_notification_time')->value;

        foreach ($candidates as $candidate)
        {
            $h = date('H:i', strtotime(" {$candidate->time_zone} hours"));

            if($h == $timeSetting){
                $message = Settings::findByKey('first_message')->value;

                self::sendTelMessage($candidate->telegram_id, $message);
            }
        }
    }

    /**
     * Принимает информацию о новом реферальном посещении
     * @var BotSetting $botSetting
     */
    public static function sendTelMessage($userId, $text)
    {
        $token = Settings::findByKey('telegram_token')->value;
//        $proxy_server = Settings::findByKey('proxy')->value;

        if(is_array($userId)){
            $result = [];
            foreach ($userId as $id) {
                $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
                $url=$url.'?'.http_build_query(['chat_id' => $id, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL,$url);
//                curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
                //curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_HEADER, 1);
                $curl_scraped_page = curl_exec($ch);
                curl_close($ch);


//                curl_setopt($curl, CURLOPT_PROXY, $proxy);
                $result[] = $curl_scraped_page;

            }

            return $result;
        } else {
            $url = 'https://api.telegram.org/bot'.$token.'/sendMessage';
            $url=$url.'?'.http_build_query(['chat_id' => $userId, 'text' => $text, 'parse_mode' => 'HTML']);//к нему мы прибавляем парметры, в виде GET-параметров

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL,$url);
//            curl_setopt($ch, CURLOPT_PROXY, $proxy_server);
//            curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            $curl_scraped_page = curl_exec($ch);
            curl_close($ch);

            return $curl_scraped_page;
        }


//        return true;
    }
}
