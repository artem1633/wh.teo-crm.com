<?php
/**
 * Created by PhpStorm.
 * User: Ilusha
 * Date: 17.12.2017
 * Time: 2:46
 */

namespace app\source\inflaters;

/**
 * Interface IInflatable
 * @package app\source\inflaters
 * Необхадим для работы с тегами
 */
interface IInflatable
{
    public function tags();
}