<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Faq */
?>
<div class="faq-view">

    <div class="panel panel-inverse">
        <div class="panel-body">
            <?php if ($model): ?>
                <?= $model->text ?>
            <?php else: ?>
                <p>No F.A.Q.</p>
            <?php endif; ?>
        </div>
    </div>
</div>
