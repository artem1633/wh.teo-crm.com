<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Candidate;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'state',
        'value' => 'stateName'
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'bank',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'account',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'checking_number',
    ],
    [
        'class' => \kartik\grid\EditableColumn::className(),
        'editableOptions' => [
            'inputType' => \kartik\editable\Editable::INPUT_SELECT2,
            'options' => [
                'data' => Candidate::statusLabels()
            ],
            'formOptions' => [
                'action' => \yii\helpers\Url::to(['edit'])
            ]
        ],
//        'inputType' => \kartik\editable\Editable::INPUT_DROPDOWN_LIST,
        'attribute' => 'status',
        'content' => function($model){
            if(isset(Candidate::statusLabels()[$model->status])){
                return Candidate::statusLabels()[$model->status];
            }

            return null;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'routing_number',
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'address',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{view} {update}{delete}',
        'buttons' => [
            'view' => function($url, $model){
                return Html::a('<i class="fa fa-eye" style="font-size: 16px;"></i>', $url, ['data-pjax' => 0, 'title' => 'view']);
            },
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'delete',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Confirm?',
                    'data-confirm-message'=>'Delete that candidate?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'edit',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   