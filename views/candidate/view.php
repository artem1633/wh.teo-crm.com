<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Candidate */

$this->title = 'Candidate page';

?>
<div class="candidate-view">

    <div class="row">
        <div class="col-md-7">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Информация</h4>
                </div>
                <div class="panel-body">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'state',
                            'bank',
                            'account',
                            'checking_number',
                            'routing_number',
                            'address',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-5">
            <div class="panel panel-inverse">
                <div class="panel-heading">
                    <h4 class="panel-title">Файлы</h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4 style="margin-top: 10px;">Files</h4>
                            <?= $this->render('@app/views/candidate-file/index', [
                                'searchModel' => $filesSearchModel,
                                'dataProvider' => $filesDataProvider,
                            ]) ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <?= \kato\DropZone::widget([
                                'id'        => 'dzImage', // <-- уникальные id
                                'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file', 'candidate_id' => $model->id ]),
                                'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
                                'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
                                'options' => [
                                    'maxFilesize' => '2',
                                ],
                                'clientEvents' => [
                                    'complete' => "function(file){ $.pjax.reload('#file-pjax'); }",
                                ],
                            ]);?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <?= $this->render('_request-view', [
                'searchModel' => $requestSearchModel,
                'dataProvider' => $requestDataProvider,
            ]) ?>
        </div>
    </div>

</div>

</div>
