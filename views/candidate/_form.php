<?php
use app\models\BankName;
use app\models\CandidateSearch;
use app\models\Request;
use app\models\RequestSearch;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Candidate */
/* @var $form yii\widgets\ActiveForm */



$searchModel = new CandidateSearch();
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
$models = $dataProvider->models;

//$banks = ArrayHelper::getColumn($models, 'bank');
$defaultBanks = ArrayHelper::getColumn(BankName::find()->all(),'name');

//$banks = array_unique(ArrayHelper::merge($banks, $defaultBanks));

$banks = array_combine($defaultBanks, $defaultBanks);

?>

<div class="candidate-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'state')->widget(\kartik\select2\Select2::className(), [
        'data' => ArrayHelper::map(\app\models\State::find()->all(), 'alias', 'name'),
    ]) ?>

    <?= $form->field($model, 'status')->dropDownList(\app\models\Candidate::statusLabels()) ?>

    <?php
//    echo $form->field($model, 'bank')->widget(\kartik\select2\Select2::class, [
//            'data' => $model->bank != null ? array_unique(\yii\helpers\ArrayHelper::merge([
//                'Chase' => 'Chase',
//                'Bank of America' => 'Bank of America',
//                'Wells Fargo' => 'Wells Fargo',
//                'Citi' => 'Citi',
//                'PNC' => 'PNC',
//                'Capital One' => 'Capital One',
//                'TD Bank' => 'TD Bank',
//                'Another' => 'Another'
//            ], array_combine($model->bank, $model->bank))) : [
//                'Chase' => 'Chase',
//                'Bank of America' => 'Bank of America',
//                'Wells Fargo' => 'Wells Fargo',
//                'Citi' => 'Citi',
//                'PNC' => 'PNC',
//                'Capital One' => 'Capital One',
//                'TD Bank' => 'TD Bank',
//                'Another' => 'Another'
//            ],
//        'options' => ['multiple' => true,],
//        'pluginOptions' => [
//            'tags' => true,
//            'tokenSeparators' => [','],
//        ],
//    ])
    ?>

    <?= $form->field($model, 'bank')->widget(\kartik\select2\Select2::class, [
        'data' => $banks,
//        'options' => ['multiple' => true,],
        'pluginOptions' => [
            'tags' => true,
            'tokenSeparators' => [','],
        ],
    ])?>

    <?= $form->field($model, 'account')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'checking_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'routing_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
        <?= $form->field($model, 'telegram_id')->textInput() ?>

        <?= $form->field($model, 'time_zone')->input('number') ?>
    <?php endif; ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
