<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CandidateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse candidate-index">
    <div class="panel-heading">
        <!--        <div class="panel-heading-btn">-->
        <!--        </div>-->
        <h4 class="panel-title">Requests</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
                'id'=>'crud-datatable',
                'dataProvider' => $dataProvider,
//                'filterModel' => $searchModel,
                'pjax'=>true,
                'columns' => [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'width' => '30px',
                    ],
                    // [
                    // 'class'=>'\kartik\grid\DataColumn',
                    // 'attribute'=>'id',
                    // ],
//                    [
//                        'class'=>'\kartik\grid\DataColumn',
//                        'attribute'=>'status',
//                        'content' => function($data) {
//                            if(isset(\app\models\Request::statusLabels()[$data->status])){
//                                return \app\models\Request::statusLabels()[$data->status];
//                            }
//
//                            return null;
//                        },
//                    ],
                    [
                        'class' => '\kartik\grid\DataColumn',
                        'label' => 'Client',
                        'attribute' => 'client_id',
                        'value' => 'client.name',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'zip',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'state',
                        'value' => 'stateName'
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'bank',
                        'content' => function($model){
                            $html = $model->bank.' ';

                            if($model->is_exists == 1){
                                $html .= '<i class="fa fa-check text-success"></i>';
                            } else if($model->is_exists == 0){
                                $html .= '<i class="fa fa-times text-danger"></i>';
                            }

                            return $html;
                        }
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'sum',
                    ],
                    [
                        'class'=>'\kartik\grid\DataColumn',
                        'attribute'=>'created_at',
                    ],
//                    [
//                        'class' => 'kartik\grid\ActionColumn',
//                        'visible' => Yii::$app->user->identity->isSuperAdmin(),
//                        'dropdown' => false,
//                        'vAlign'=>'middle',
//                        'urlCreator' => function($action, $model, $key, $index) {
//                            return Url::to([$action,'id'=>$key]);
//                        },
//                        'template' => '{view-files} {fail} {update}{delete}',
//                        'buttons' => [
//                            'fail' => function($url, $model){
//                                if(in_array($model->status, [Request::STATUS_CLOSED, Request::STATUS_FAILED]) == false){
//                                    return Html::a('<i class="fa fa-archive"></i>', $url, [
////                        'class' => 'btn btn-white btn-xs',
//                                        'class' => 'text-success',
//                                        'role' => 'modal-remote'
//                                    ]);
//                                }
//                            },
//                            'delete' => function ($url, $model) {
//                                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
//                                    'role'=>'modal-remote', 'title'=>'Delete',
//                                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                                    'data-request-method'=>'post',
//                                    'data-confirm-title'=>'Confirm?',
//                                    'data-confirm-message'=>'Delete that request?'
//                                ]);
//                            },
//                            'update' => function ($url, $model) {
//                                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
//                                        'role'=>'modal-remote', 'title'=>'Edit',
//                                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
//                                        'data-request-method'=>'post',
//                                    ])."&nbsp;";
//                            }
//                        ],
//                    ],

                ],
                'panelBeforeTemplate' => '',
                'striped' => true,
                'condensed' => true,
                'responsiveWrap' => false,
//            'responsive' => true,
                'panel' => [
                    'headingOptions' => ['style' => 'display: none;'],
                    'after'=>'',
                ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    'options' => [
        'tabindex' => false,
    ],
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
