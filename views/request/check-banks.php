<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Candidate;

/* @var $this yii\web\View */
/* @var $model \app\models\Request */
/* @var $request \app\models\Request */
/* @var $form yii\widgets\ActiveForm */
/* @var $formModel \app\models\forms\CandidateForm */

//$banks = Yii::$app->user->identity->candidate->bank;

//if($banks != null){
//    $banks = explode(',', $banks);
//}

$banks = \yii\helpers\ArrayHelper::map(Candidate::find()->where(['user_id' => Yii::$app->user->identity->id])->all(), 'id', 'bank');


//\yii\helpers\VarDumper::dump(Candidate::find()->where(['user_id' => $model->id])->all(), 10, true);

$banksCount = 0;

?>

<div class="request-form">

    <?php $form = ActiveForm::begin(); ?>

    <table class="table table-bordered">
        <tbody>
            <?php foreach ($banks as $bankId => $bank): ?>
                <?php
                $candidate = Candidate::findOne($bankId);
                if($bank == $model->bank){
                    continue;
                }
                if($model->state == $candidate->state){
                    continue;
                }

                $banksCount = $banksCount + 1;
                ?>
                <tr>
                    <td style="vertical-align: middle; text-align: center; width: 5%;">
                        <input type="radio" name="CandidateForm[candidateId]" value="<?=$bankId?>" data-bank="<?=$bank?>" disabled="">
                    </td>
                    <td style="vertical-align: middle; font-size: 15px;">
                        <?= $bank ?>
                    </td>
                    <td>
                        <?= Html::a('Check', ['check-bank', 'zip' => $model->zip, 'bank' => $bank], ['class' => 'btn-checker btn btn-warning btn-sm btn-block', 'data-bank' => $bank]) ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="hidden">
        <?= Html::a('', ['request/next-candidate', 'id' => $model->id], ['id' => 'next-candidate-link', 'role' => 'modal-remote']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php


$script = <<< JS
var remainBanks = {$banksCount};

$('.btn-checker').click(function(e){
    e.preventDefault();
    
    var bank = $(this).data('bank');
    var html = $(this).html() + ' <i class="fa fa-spinner fa-spin"></i>';
    
    $(this).html(html);
    
    var url = $(this).attr('href');
    
    var self = this;
    
    $.get(url, function(response){
        console.log(response);
        if(response.result === true){
            $(self).html('AVAILABLE');
            $(self).removeClass('btn-warning');
            $(self).addClass('btn-success');
            $('input[data-bank="'+bank+'"]').removeAttr('disabled');
        } else {
            $(self).html('NOT AVAILABLE');
            remainBanks = remainBanks - 1;
            $(self).removeClass('btn-warning');
            $(self).addClass('btn-danger');
            if(remainBanks == 0){
                $('#next-candidate-link').trigger('click');
            }
        }
        $(self).unbind('click');
        $(self).attr('href', '#');
    });
    
});

JS;

$this->registerJs($script, \yii\web\View::POS_READY)

?>