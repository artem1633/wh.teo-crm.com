<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 
use johnitvn\ajaxcrud\BulkButtonWidget;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RequestSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Requests";
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

if(Yii::$app->user->identity->isSuperAdmin()){
    if($searchModel->showHidden == 1){
        $archiveBtn = Html::a('Show all <i class="fa fa-archive"></i>', ['index', 'RequestSearch[showHidden]' => '0'], ['class' => 'btn btn-info']) . '&nbsp;';
    } else {
        $archiveBtn = Html::a('Archive <i class="fa fa-archive"></i>', ['index', 'RequestSearch[showHidden]' => '1'], ['class' => 'btn btn-white']) . '&nbsp;';
    }
} else {
    $archiveBtn = '';
}

$after = '';

if(Yii::$app->user->identity->isSuperAdmin()){
    $after = BulkButtonWidget::widget([
            'buttons'=>Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Delete',
                ["bulk-delete"] ,
                [
                    "class"=>"btn btn-danger btn-xs",
                    'role'=>'modal-remote-bulk',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Confirm?',
                    'data-confirm-message'=>'Delete elements?'
                ]),
        ]).
        '<div class="clearfix"></div>';
}

$panelBeforeTemplate = '';

if(Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->isClient()){
    $panelBeforeTemplate = Html::a('Add <i class="fa fa-plus"></i>', ['create'],
            ['role'=>'modal-remote','title'=> 'Add request','class'=>'btn btn-success']).'&nbsp;'.
        $archiveBtn;
}

?>
<div class="panel panel-inverse request-index">
    <div class="panel-heading">
<!--        <div class="panel-heading-btn">-->
<!--        </div>-->
        <h4 class="panel-title">Requests</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?=GridView::widget([
            'id'=>'crud-datatable',
            'dataProvider' => $dataProvider,
            'pjax'=>true,
            'columns' => require(__DIR__.'/_columns.php'),
            'rowOptions' => function($model){
                if($model->status == \app\models\Request::STATUS_DOCS_LOADED){
                    return ['class' => 'success'];
//                    return ['class' => 'receive'];
                }

                if($model->status == \app\models\Request::STATUS_DONE){
//                    return ['class' => 'info'];
                    return ['class' => 'done'];
                }

                if($model->status == \app\models\Request::STATUS_CLOSED){
                    return ['class' => 'default'];
                }

                if($model->status == \app\models\Request::STATUS_WAITING){
                    return ['class' => 'waiting'];
                }

                if($model->status == \app\models\Request::STATUS_WORK){
                    return ['class' => 'work'];
                }

                return ['class' => 'warning'];
            },
            'panelBeforeTemplate' => $panelBeforeTemplate,
            'striped' => true,
            'condensed' => true,
            'responsiveWrap' => false,
            'panel' => [
            'headingOptions' => ['style' => 'display: none;'],
            'after'=>$after,
            ]
            ])?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>
