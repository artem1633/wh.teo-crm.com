<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Candidate;

/* @var $this yii\web\View */
/* @var $model \app\models\forms\CandidateForm */
/* @var $request \app\models\Request */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'candidateId')->widget(\kartik\select2\Select2::className(), [
        'data' => \yii\helpers\ArrayHelper::map(
            Candidate::find()
                ->andWhere(['!=', 'bank', $request->bank])
                ->andWhere(['!=', 'state', $request->state])
                ->andWhere(['status' => Candidate::STATUS_READY])
                ->all(),
            'id', 'account'),
    ]) ?>

    <?php ActiveForm::end(); ?>

</div>
