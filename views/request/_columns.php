<?php

use app\models\RequestFile;
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Request;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
        'visible' => Yii::$app->user->identity->isSuperAdmin()
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'client_id',
        'label' => 'Client',
        'content' => function (Request $model) {
            if(Yii::$app->user->identity->isCandidate() == false){
                return Html::a($model->client->name ?? null, ['/user/view', 'id' => $model->client_id],
                    [
                        'role' => 'modal-remote'
//                    'data-pjax' => 0,
                    ]);
            } else {
                return $model->client->name;
            }

        },
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'status',
        'content' => function ($data) {
            if (isset(\app\models\Request::statusLabels()[$data->status])) {
                return \app\models\Request::statusLabels()[$data->status];
            }

            return null;
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'zip',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'state',
        'value' => 'stateName'
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'bank',
        'content' => function ($model) {
            $html = $model->bank . ' ';

            if ($model->is_exists == 1) {
                $html .= '<i class="fa fa-check text-success"></i>';
            } else {
                if ($model->is_exists == 0) {
                    $html .= '<i class="fa fa-times text-danger"></i>';
                }
            }

            if(Yii::$app->user->identity->isCandidate() && $model->status == Request::STATUS_NEW){
                $html .= '<p style="margin-bottom: 0;">'.Html::a('Check', ['check-banks', 'id' => $model->id], ['class' => 'btn btn-success btn-xs btn-block', 'role' => 'modal-remote']).'</p>';
            }

            return $html;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'sum',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Timer',
        'content' => function ($model) {
            return $model->timer;
        },
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'recs',
        'visible' => Yii::$app->user->identity->isCandidate() == false,
        'format' => 'html',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'recs_payment',
        'content' => function ($model) {
            if (in_array($model->status, [Request::STATUS_CLOSED, Request::STATUS_DONE])) {
                return $model->recs_payment . '<br>' . Html::a('<i class="fa fa-pencil"></i>',
                        ['update-recs-payment', 'id' => $model->id], [
                            'class' => 'btn btn-primary btn-xs',
                            'role' => 'modal-remote'
                        ]);
            }
        }
    ],
    [
        'attribute' => 'sum_payment',
        'visible' => !Yii::$app->user->identity->isCandidate()
    ],
    [
        'label' => 'Candidate',
        'visible' => Yii::$app->user->identity->isCandidate(),
        'content' => function ($model) {
            if ($model->candidate_id != null) {
                return $model->candidate->bank;
            }
        },
        'label' => 'Bank',
        'width' => '10px'
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'sum',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'status_changed_at',
    // ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'created_at',
    // ],
    [
        'label' => 'Done',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function ($model) {

            if ($model->status == Request::STATUS_DONE) {
                return Html::a('Payment', ['payment', 'id' => $model->id], [
                    'class' => 'btn btn-success btn-sm',
                    'role' => 'modal-remote',
                    'title' => 'Payment',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-confirm-title' => 'Confirm',
                    'data-confirm-message' => 'Set payment timer?'
                ]);
            }

            if ($model->status == Request::STATUS_PAYMENT) {
                return Html::a('Close', ['close', 'id' => $model->id], [
                    'class' => 'btn btn-default btn-sm',
                    'role' => 'modal-remote',
                    'title' => 'Make close',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-confirm-title' => 'Confirm',
                    'data-confirm-message' => 'Close that request?'
                ]);
            }
        },
    ],
    [
        'label' => 'Candidate',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'content' => function ($model) {
            if ($model->candidate_id != null) {
                return "<p>Current candidate: {$model->candidate->account}</p>" . Html::a('Candidate',
                        ['apply-candidate', 'id' => $model->id], [
                            'class' => 'btn btn-success btn-sm',
                            'role' => 'modal-remote'
                        ]);
            } else {
                return Html::a('Candidate', ['apply-candidate', 'id' => $model->id], [
                    'class' => 'btn btn-success btn-sm',
                    'role' => 'modal-remote'
                ]);
            }
        },
        'width' => '10px'
    ],
    [
        'label' => 'Checks',
        'content' => function ($model) {
            $output = '';

            if(Yii::$app->user->identity->isClient()){
                $output = Html::a('Checks', ['view-files', 'id' => $model->id], [
                    'class' => 'btn btn-success btn-sm btn-block',
                    'role' => 'modal-remote'
                ]);
            }

            if(Yii::$app->user->identity->isCandidate() && RequestFile::find()->where(['request_id' => $model->id])->count() > 0 && $model->receive_date == null){
                $output .= Html::a('Receive', ['receive', 'id' => $model->id], [
                    'class' => 'btn btn-info btn-sm btn-block',
                    'role' => 'modal-remote',
                    'title' => 'Receive',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-confirm-title' => 'Confirm',
                    'data-confirm-message' => 'Receive?'
                ]);
            }

            if(Yii::$app->user->identity->isCandidate() && $model->status == Request::STATUS_DOCS_LOADED){
                $output .= Html::a('In work', ['to-work', 'id' => $model->id], [
                    'class' => 'btn btn-warning btn-sm btn-block',
                    'role' => 'modal-remote',
                    'title' => 'Receive',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-confirm-title' => 'Confirm',
                    'data-confirm-message' => 'To work?'
                ]);
            }

            if (Yii::$app->user->identity->isCandidate() && in_array($model->status, [Request::STATUS_NEW, Request::STATUS_WORK])) {
                return Html::a('Done', ['done', 'id' => $model->id], [
                    'class' => 'btn btn-info btn-sm',
                    'role' => 'modal-remote',
//                    'title' => 'Make done',
//                    'data-confirm' => false,
//                    'data-method' => false,// for overide yii data api
//                    'data-request-method' => 'post',
//                    'data-confirm-title' => 'Confirm',
//                    'data-confirm-message' => 'Mark as done that request?'
                ]);
            }

            return $output;
        },
        'width' => '10px'
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'template' => '{view-files} {fail} {update}{delete}',
        'buttons' => [
            'fail' => function ($url, $model) {
                if (in_array($model->status, [Request::STATUS_CLOSED, Request::STATUS_FAILED]) == false) {
                    return Html::a('<i class="fa fa-archive"></i>', $url, [
//                        'class' => 'btn btn-white btn-xs',
                        'class' => 'text-success',
                        'role' => 'modal-remote'
                    ]);
                }
            },
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role' => 'modal-remote',
                    'title' => 'Delete',
                    'data-confirm' => false,
                    'data-method' => false,// for overide yii data api
                    'data-request-method' => 'post',
                    'data-confirm-title' => 'Confirm?',
                    'data-confirm-message' => 'Delete that request?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                        'role' => 'modal-remote',
                        'title' => 'Edit',
                        'data-confirm' => false,
                        'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                    ]) . "&nbsp;";
            }
        ],
    ],

];   