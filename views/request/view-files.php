
<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'sum')->input('number') ?>

    <?php ActiveForm::end(); ?>

</div>


<div class="row">
    <div class="col-md-12">
        <h4 style="margin-top: 10px;">Files</h4>
        <?= $this->render('@app/views/request-file/index', [
            'searchModel' => $filesSearchModel,
            'dataProvider' => $filesDataProvider,
        ]) ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <?= \kato\DropZone::widget([
            'id'        => 'dzImage', // <-- уникальные id
            'uploadUrl' => \yii\helpers\Url::toRoute([ 'upload-file', 'id' => $model->id ]),
            'dropzoneContainer' => 'dz-container-images', // <-- уникальные dropzoneContainer
            'previewsContainer' => 'preview-images', // <-- уникальные previewsContainer
            'options' => [
                'maxFilesize' => '20',
            ],
            'clientEvents' => [
                'complete' => "function(file){ $.pjax.reload('#file-pjax'); }",
            ],
        ]);?>
    </div>
</div>