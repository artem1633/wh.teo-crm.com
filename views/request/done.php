<?php
use kartik\datetime\DateTimePicker;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'done_sum')->textInput() ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'done_datetime')->widget(DateTimePicker::class, [

            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
