<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Request */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="request-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'state')->widget(\kartik\select2\Select2::className(), [
        'data' => \yii\helpers\ArrayHelper::map(\app\models\State::find()->all(), 'alias', 'name'),
    ]) ?>

    <?= $form->field($model, 'zip')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'bank')->dropDownList([
        'Chase' => 'Chase',
        'Bank of America' => 'Bank of America',
        'Wells Fargo' => 'Wells Fargo',
        'Citi' => 'Citi',
        'PNC' => 'PNC',
        'Capital One' => 'Capital One',
        'TD Bank' => 'TD Bank',
        'Another' => 'Another'
    ]) ?>

    <?= $form->field($model, 'sum')->input('number') ?>

    <?php if(Yii::$app->user->identity->isSuperAdmin()): ?>
        <?= $form->field($model, 'sum_payment')->input('number') ?>

        <?= $form->field($model, 'done_datetime')->widget(\kartik\datetime\DateTimePicker::className(), [

        ]) ?>
    <?php endif; ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
