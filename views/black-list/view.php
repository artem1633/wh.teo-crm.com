<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BlackList */
?>
<div class="black-list-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'user_id',
                    'value' => $model->getAuthorInfo(),
                    'visible' => Yii::$app->user->identity->isSuperAdmin(),
                    'format' => 'raw',
                ],
                'name:ntext',
                'state:ntext',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), 'error');
        echo $e->getMessage();
    } ?>

</div>
