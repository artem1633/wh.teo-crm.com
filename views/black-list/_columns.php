<?php

use app\models\BlackList;
use yii\helpers\Html;
use yii\helpers\Url;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'user_info',
        'content' => function (BlackList $model) {
           return $model->getAuthorInfo();
        },
        'label' => 'Author',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'name',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'state',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'buttons' => [
            'update' => function ($url, BlackList $model) {
                return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [$url], [
                    'class' => $model->isRecordAuthor() ? '' : 'hidden',
                    'role' => 'modal-remote',
                    'title' => 'Update',
                    'data-toggle' => 'tooltip',
                ]);
            }
        ],
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => [
            'role' => 'modal-remote',
            'title' => 'Update',
            'data-toggle' => 'tooltip',
        ],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item',
            'class' => Yii::$app->user->identity->isSuperAdmin() ? '' : 'hidden',
        ],
    ],

];   