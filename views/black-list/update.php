<?php

/* @var $this yii\web\View */
/* @var $model app\models\BlackList */
?>
<div class="black-list-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
