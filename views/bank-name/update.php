<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BankName */
?>
<div class="bank-name-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
