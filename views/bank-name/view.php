<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BankName */
?>
<div class="bank-name-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
        ],
    ]) ?>

</div>
