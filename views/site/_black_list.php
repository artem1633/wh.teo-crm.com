<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BlackList */
/* @var $form ActiveForm */
?>
<div class="site-_black_list">

    <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'data') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-_black_list -->
