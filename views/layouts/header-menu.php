<?php

use app\admintheme\widgets\TopMenu;
use app\models\Ticket;

if(Yii::$app->user->identity->isSuperAdmin()){
    $unreadTicketCount = Ticket::find()->where(['is_read' => Ticket::READ_ADMIN])
        ->andWhere(['not in', 'status', [Ticket::STATUS_DONE, Ticket::STATUS_REJECTED]])->count();
} else {
    $unreadTicketCount = Ticket::find()->where(['is_read' => Ticket::READ_USER])->andWhere(['user_id' => Yii::$app->user->getId()])
        ->andWhere(['not in', 'status', [Ticket::STATUS_DONE, Ticket::STATUS_REJECTED]])->count();
}

if($unreadTicketCount == 0){
    $unreadTicketLabel = "Ticket";
} else {
    $unreadTicketLabel = "Ticket <span class='label label-primary pull-right' title='Unread tickets' style='font-size: 11px; margin-left: 10px; margin-top: 2px;'>{$unreadTicketCount}</span>";
}

?>

<div id="top-menu" class="top-menu">
    <?php if(Yii::$app->user->isGuest == false): ?>
        <?php
        try {
            echo TopMenu::widget(
                [
                    'encodeLabels' => false,
                    'options' => ['class' => 'nav'],
                    'items' => [
                        [
                            'label' => 'Users',
                            'icon' => 'fa  fa-users',
                            'url' => ['/user'],
                            'visible' => Yii::$app->user->identity->isSuperAdmin()
                        ],
                        [
                            'label' => 'Banks',
                            'icon' => 'fa  fa-user-o',
                            'url' => ['/candidate'],
                            'visible' => (Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->isCandidate())
                        ],
                        ['label' => 'Requests', 'icon' => 'fa  fa-file', 'url' => ['/request'],],
                        [
                            'label' => 'Report',
                            'icon' => 'fa  fa-area-chart',
                            'url' => ['/report'],
                            'visible' => Yii::$app->user->identity->isSuperAdmin()
                        ],
                        [
                            'label' => 'Settings',
                            'icon' => 'fa  fa-cog',
                            'url' => ['/settings'],
                            'visible' => Yii::$app->user->identity->isSuperAdmin()
                        ],
                        ['label' => $unreadTicketLabel, 'icon' => 'fa  fa-question', 'url' => ['/ticket'],],
                        ['label' => 'FAQ', 'icon' => 'fa  fa-file-text-o', 'url' => ['/faq'],
                            'visible' => Yii::$app->user->identity->isSuperAdmin() || Yii::$app->user->identity->isCandidate(),
                            ],
                        ['label' => 'Black List', 'icon' => 'fa  fa-ban', 'url' => ['/black-list'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                        ['label' => 'Bank names', 'icon' => 'fa  fa-book', 'url' => ['/bank-name'], 'visible' => Yii::$app->user->identity->isSuperAdmin()],
                    ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), 'error');
            echo $e->getMessage();
        }
        ?>
    <?php endif; ?>
</div>
