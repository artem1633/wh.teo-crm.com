<?php

use yii\helpers\Html;
use app\models\Users;

$data = json_decode(file_get_contents("http://preev.com/pulse/units:btc+usd/sources:bitstamp+kraken"), true);

if(isset($data['btc']['usd']['bitstamp']['last'])){
    $usd = $data['btc']['usd']['bitstamp']['last'];
}

?>

<div id="header" class="header navbar navbar-default navbar-fixed-top">
            <!-- begin container-fluid -->
            <div class="container-fluid">
                <!-- begin mobile sidebar expand / collapse button -->
                <div class="navbar-header">
                    <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand">
                        <span class="navbar-logo"></span>
                        <?=Yii::$app->name?>
                    </a>
                    <button type="button" class="navbar-toggle" data-click="top-menu-toggled">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- end mobile sidebar expand / collapse button -->


                <?php if(Yii::$app->user->isGuest == false): ?>

                    <ul class="nav navbar-nav" style="width: 9%;">
                        <b style="margin-top: 14px; display: inline-block; font-size: 18px; margin-left: -100px;"><?= Yii::$app->formatter->asDatetime(time(), 'php:h:i A') ?></b>
                        <input type="text" class="form-control" data-current-price="<?=$usd?>" style="margin-top: -28px; width: 80px;" placeholder="USD" onkeyup="calculate($(this));">
                        <p id="calculate-value" style="float: right; margin-top: -25px;"></p>
                    </ul>

                    <!-- begin header navigation right -->
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown navbar-user">
                            <a id="btn-dropdown_header" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                <span class="hidden-xs"><?=Yii::$app->user->identity->login?></span> <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu animated fadeInLeft">
                                <li class="arrow"></li>
                                <?php if(Yii::$app->user->identity->isCandidate()): ?>
                                    <li> <?= Html::a('Edit', ['/candidate/update', 'id' => Yii::$app->user->identity->candidate_id], ['role' => 'modal-remote']) ?> </li>
                                <?php endif; ?>
                                <li> <?= Html::a('Logout', ['/site/logout'], ['data-method' => 'post']) ?> </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- end header navigation right -->

                <?php endif; ?>
            </div>
            <!-- end container-fluid -->
        </div>