<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User */

$this->title = "User page";

?>
<div class="user-view">

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Информация</h4>
            </div>
            <div class="panel-body">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'login',
                        'name',
//                        'password_hash',
//                        'password',
//                        'is_deletable',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <?= $this->render('_request-view', [
            'searchModel' => $requestSearchModel,
            'dataProvider' => $requestDataProvider,
        ]) ?>
    </div>
</div>

</div>
