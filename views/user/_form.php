<?php
use app\models\User;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Candidate;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */


?>


<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'login')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'role')->dropDownList(User::roleLabels()) ?>

    <?= $form->field($model, 'password')->textInput(['maxlength' => true, 'data-toggle' => 'tooltip']) ?>

    <?= $form->field($model, 'telegram_id')->textInput(['maxlength' => true, 'data-toggle' => 'tooltip']) ?>

    <?= $form->field($model, 'candidate_visible_sort')->textInput(['maxlength' => true, 'data-toggle' => 'tooltip']) ?>


	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>


<?php
//
//$script = <<< JS
//$('#user-role').change(function(){
//    if($(this).val() == 2){
//        $('#candidate-wrapper').show();
//    } else {
//        $('#candidate-wrapper').hide();
//        $('#user-candidate_id').val(null);
//    }
//});
//JS;
//
//$this->registerJs($script, \yii\web\View::POS_READY);

?>