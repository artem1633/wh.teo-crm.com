<?php
use yii\helpers\Url;
use yii\helpers\Html;
use app\models\Ticket;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'label' => 'Read',
        'content' => function($model){
            return Html::a('<i class="fa fa-envelope text-info" style="font-size: 17px;"></i> ', ['view', 'id' => $model->id], ['data-pjax' => 0]);
        },
        'width' => '10px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'user_id',
        'value' => 'user.name',
        'visible' => Yii::$app->user->identity->isSuperAdmin(),
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'subject',
        'content' => function($model){
            $html = '';

            if($model->hasUnread()){
                $html .= '<i class="fa fa-envelope text-primary" style="font-size: 16px;"></i> '.$model->subject;
            }

            $html .= $model->subject;

            return $html;
        },
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'description',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'status',
        'content' => function($model){
            if(isset(Ticket::getStatuses()[$model->status])){
                return Ticket::getStatuses()[$model->status];
            }
        },
    ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'last_message_datetime',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'created_at',
    // ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'closed_datetime',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template' => '{update}{delete}',
        'buttons' => [
            'delete' => function ($url, $model) {
                return Html::a('<i class="fa fa-trash text-danger" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Delete',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                    'data-confirm-title'=>'Confirm',
                    'data-confirm-message'=>'Do you want delete this record?'
                ]);
            },
            'update' => function ($url, $model) {
                return Html::a('<i class="fa fa-pencil text-primary" style="font-size: 16px;"></i>', $url, [
                    'role'=>'modal-remote', 'title'=>'Edit',
                    'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                    'data-request-method'=>'post',
                ])."&nbsp;";
            }
        ],
    ],

];   