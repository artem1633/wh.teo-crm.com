<?php

use kartik\grid\GridView;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $model \app\models\forms\ReportForm
 * @var $clientDataProvider \yii\data\ArrayDataProvider
 * @var $candidateDataProvider \yii\data\ArrayDataProvider
 */

$this->title = 'Report';

$dates = $model->dates;

$model->dates = null;

?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Filters</h4>
            </div>
            <div class="panel-body">
                <?php $form = \yii\widgets\ActiveForm::begin(['id' => 'filter-form', 'method' => 'GET']) ?>
                <div class="row">
                    <div class="col-md-3">
                        <?= $form->field($model, 'dates')->widget(\kartik\daterange\DateRangePicker::className(), [
                            'options' => [
                                'class' => 'form-control',
                                'autocomplete' => 'off',
                            ],
                        ]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <p>Filter Dates: <?=$dates?></p>
                    </div>
                </div>
                <?php \yii\widgets\ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Clients</h4>
            </div>
            <div class="panel-body">
                <?= GridView::widget([
                    'dataProvider' => $clientDataProvider,
                    'columns' => [
                        [
                            'attribute' => 'user_name',
                            'label' => 'Client',
                            'content' => function($data){
                                return Html::a($data['user_name'], ['user/view', 'id' => $data['user_id']]);
                            },
                        ],
                        [
                            'attribute' => 'request_count',
                            'label' => 'Requests count'
                        ],
                        [
                            'attribute' => 'request_done_count',
                            'label' => 'Done requests count'
                        ],
                        [
                            'attribute' => 'request_failed_count',
                            'label' => 'Failed requests count'
                        ],
                        [
                            'attribute' => 'request_total_sum',
                            'label' => 'Total sum'
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-inverse">
            <div class="panel-heading">
                <h4 class="panel-title">Candidates</h4>
            </div>
            <div class="panel-body">
                <?= GridView::widget([
                    'dataProvider' => $candidateDataProvider,
                    'columns' => [
                        [
                            'attribute' => 'candidate_account',
                            'label' => 'Candidate',
                            'content' => function($data){
                                return Html::a($data['candidate_account'], ['candidate/view' , 'id' => $data['candidate_id']]);
                            }
                        ],
                        [
                            'attribute' => 'request_count',
                            'label' => 'Requests count'
                        ],
                        [
                            'attribute' => 'request_total_sum',
                            'label' => 'Total sum'
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>


<?php

$script = <<< JS
$('#reportform-dates').change(function(){
    $('#filter-form').submit();
});
JS;

$this->registerJs($script, \yii\web\View::POS_READY);

?>