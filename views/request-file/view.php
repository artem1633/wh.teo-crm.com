<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RequestFile */
?>
<div class="request-file-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'request_id',
            'name',
            'path',
            'created_at',
        ],
    ]) ?>

</div>
