<?php

use yii\db\Migration;

/**
 * Handles adding telegram_id to table `user`.
 */
class m210112_094118_add_telegram_id_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'telegram_id', $this->string()->comment('Телеграм'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'telegram_id');
    }
}
