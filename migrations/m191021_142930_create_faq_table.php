<?php

use yii\db\Migration;

/**
 * Handles the creation of table `faq`.
 */
class m191021_142930_create_faq_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('faq', [
            'id' => $this->primaryKey(),
            'text' => $this->text()->comment('Текст'),
        ]);

        $this->insert('faq', [
            'text' => '',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('faq');
    }
}
