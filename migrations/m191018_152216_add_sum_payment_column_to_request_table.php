<?php

use yii\db\Migration;

/**
 * Handles adding sum_payment to table `request`.
 */
class m191018_152216_add_sum_payment_column_to_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('request', 'sum_payment', $this->float());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('request', 'sum_payment');
    }
}
