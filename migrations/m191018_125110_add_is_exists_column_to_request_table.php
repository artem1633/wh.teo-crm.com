<?php

use yii\db\Migration;

/**
 * Handles adding is_exists to table `request`.
 */
class m191018_125110_add_is_exists_column_to_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('request', 'is_exists', $this->boolean());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('request', 'is_exists');
    }
}
