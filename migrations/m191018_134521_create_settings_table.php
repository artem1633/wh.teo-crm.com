<?php

use yii\db\Migration;

/**
 * Handles the creation of table `settings`.
 */
class m191018_134521_create_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('settings', [
            'id' => $this->primaryKey(),
            'key' => $this->string()->notNull()->unique()->comment('Ключ'),
            'value' => $this->string()->comment('Значение'),
            'label' => $this->text()->comment('Комментарий'),
        ]);
        $this->addCommentOnTable('settings', 'Настройки системы');

        $this->insert('settings', [
            'key' => 'proxy',
            'label' => 'Proxy Server'
        ]);

        $this->insert('settings', [
            'key' => 'telegram_token',
            'label' => 'Telegram Token',
        ]);

        $this->insert('settings', [
            'key' => 'first_message',
            'label' => 'First Message'
        ]);

        $this->insert('settings', [
            'key' => 'second_message',
            'label' => 'Second Message'
        ]);

        $this->insert('settings', [
            'key' => 'first_timer',
            'label' => 'First Timer (min)'
        ]);

        $this->insert('settings', [
            'key' => 'second_timer',
            'label' => 'Second Timer (min)'
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('settings');
    }
}
