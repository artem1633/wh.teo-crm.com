<?php

use yii\db\Migration;

/**
 * Handles adding candidate_visible_id to table `request`.
 */
class m210112_082344_add_candidate_visible_id_column_to_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('request', 'candidate_visible_id', $this->integer()->comment('Кандидат'));

        $this->createIndex(
            'idx-request-candidate_visible_id',
            'request',
            'candidate_visible_id'
        );

        $this->addForeignKey(
            'fk-request-candidate_visible_id',
            'request',
            'candidate_visible_id',
            'candidate',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-request-candidate_visible_id',
            'request'
        );

        $this->dropIndex(
            'idx-request-candidate_visible_id',
            'request'
        );

        $this->dropColumn('request', 'candidate_visible_id');
    }
}
