<?php

use yii\db\Migration;

/**
 * Handles adding receive_date to table `request`.
 */
class m201201_131953_add_receive_date_column_to_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('request', 'receive_date', $this->date()->comment('Дата перехода в статус'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('request', 'receive_date');
    }
}
