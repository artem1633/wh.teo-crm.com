<?php

use yii\db\Migration;

/**
 * Handles the creation of table `bank_name`.
 */
class m210113_143520_create_bank_name_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bank_name', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование'),
        ]);

        $base = [
            'Chase',
            'Bank of America',
            'Wells Fargo',
            'Citi',
            'PNC',
            'Capital One',
            'TD Bank',
            'Another'
        ];

        foreach ($base as $record){
            $this->insert('bank_name', [
                'name' => $record
            ]);
        }

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('bank_name');
    }
}
