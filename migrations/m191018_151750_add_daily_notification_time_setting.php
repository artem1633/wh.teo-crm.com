<?php

use yii\db\Migration;

/**
 * Class m191018_151750_add_daily_notification_time_setting
 */
class m191018_151750_add_daily_notification_time_setting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'daily_notification_time',
            'label' => 'Daily Notification Time',
            'value' => '10:00',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    }
}
