<?php

use yii\db\Migration;

/**
 * Handles adding user_id to table `candidate`.
 */
class m201126_150238_add_user_id_column_to_candidate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('candidate', 'user_id', $this->integer()->comment('Пользователь'));

        $this->createIndex(
            'idx-candidate-user_id',
            'candidate',
            'user_id'
        );

        $this->addForeignKey(
            'fk-candidate-user_id',
            'candidate',
            'user_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-candidate-user_id',
            'candidate'
        );

        $this->dropIndex(
            'idx-candidate-user_id',
            'candidate'
        );

        $this->dropColumn('candidate', 'user_id');
    }
}
