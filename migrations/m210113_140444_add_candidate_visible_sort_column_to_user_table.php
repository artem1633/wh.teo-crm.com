<?php

use yii\db\Migration;

/**
 * Handles adding candidate_visible_sort to table `user`.
 */
class m210113_140444_add_candidate_visible_sort_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'candidate_visible_sort', $this->integer()->comment('Сортировка'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'candidate_visible_sort');
    }
}
