<?php

use yii\db\Migration;

/**
 * Handles adding done to table `request`.
 */
class m201201_140200_add_done_columns_to_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('request', 'done_sum', $this->float()->comment('Сумма'));
//        $this->addColumn('request', 'done_datetime', $this->float()->comment('Дата и время завершения'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('request', 'done_sum');
//        $this->dropColumn('request', 'done_datetime');
    }
}
