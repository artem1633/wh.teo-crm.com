<?php

use yii\db\Migration;

/**
 * Handles adding status to table `candidate`.
 */
class m191016_120651_add_status_column_to_candidate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('candidate', 'status', $this->integer()->comment('Статус'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('candidate', 'status');
    }
}
