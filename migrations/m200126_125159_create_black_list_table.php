<?php

use yii\db\Migration;

/**
 * Handles the creation of table `black_list`.
 */
class m200126_125159_create_black_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('black_list', [
            'id' => $this->primaryKey(),
            'data' => $this->text(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('black_list');
    }
}
