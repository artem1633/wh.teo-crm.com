<?php

use yii\db\Migration;

/**
 * Handles adding continued to table `request`.
 */
class m191016_154342_add_continued_column_to_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('request', 'continued', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('request', 'continued');
    }
}
