<?php

use yii\db\Migration;

/**
 * Class m200127_165058_add_columns_to_create_table
 */
class m200127_165058_add_columns_to_black_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->renameColumn('black_list', 'data', 'name');
        $this->addColumn('black_list', 'state', $this->string());
        $this->addColumn('black_list', 'user_id', $this->integer()->comment('Author of the record'));
        $this->addColumn('black_list', 'created_at', $this->timestamp()->defaultExpression('NOW()'));

        $this->addForeignKey(
            'fk-black_list-user_id',
            'black_list',
            'user_id',
            'user',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        echo "m200127_165058_add_columns_to_create_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200127_165058_add_columns_to_create_table cannot be reverted.\n";

        return false;
    }
    */
}
