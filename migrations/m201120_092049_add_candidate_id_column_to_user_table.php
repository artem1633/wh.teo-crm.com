<?php

use yii\db\Migration;

/**
 * Handles adding candidate_id to table `user`.
 */
class m201120_092049_add_candidate_id_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'candidate_id', $this->integer()->comment('Кандидат'));

        $this->createIndex(
            'idx-user-candidate_id',
            'user',
            'candidate_id'
        );

        $this->addForeignKey(
            'fk-user-candidate_id',
            'user',
            'candidate_id',
            'candidate',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-user-candidate_id',
            'user'
        );

        $this->dropIndex(
            'idx-user-candidate_id',
            'user'
        );

        $this->dropColumn('user', 'candidate_id');
    }
}
