<?php

use yii\db\Migration;

/**
 * Class m191015_173607_add_column_to_request_table
 */
class m191015_173607_add_column_to_request_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('request', 'candidate_id', $this->integer());
        $this->addColumn('request', 'recs', $this->text());
        $this->addColumn('request', 'recs_payment', $this->text());
        $this->addColumn('request', 'done_datetime', $this->dateTime());

        $this->createIndex(
            'idx-request-candidate_id',
            'request',
            'candidate_id'
        );

        $this->addForeignKey(
            'fk-request-candidate_id',
            'request',
            'candidate_id',
            'candidate',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-request-candidate_id',
            'request'
        );

        $this->dropIndex(
            'idx-request-candidate_id',
            'request'
        );

        $this->dropColumn('request', 'recs_payment');
        $this->dropColumn('request', 'done_datetime');
        $this->dropColumn('request', 'recs');
        $this->dropColumn('request', 'candidate_id');
    }

}
