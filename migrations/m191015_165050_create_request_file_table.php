<?php

use yii\db\Migration;

/**
 * Handles the creation of table `request_file`.
 */
class m191015_165050_create_request_file_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('request_file', [
            'id' => $this->primaryKey(),
            'request_id' => $this->integer(),
            'name' => $this->string()->comment('Наименование файла'),
            'path' => $this->string()->comment('Путь'),
            'created_at' => $this->dateTime()
        ]);

        $this->createIndex(
            'idx-request_file-request_id',
            'request_file',
            'request_id'
        );

        $this->addForeignKey(
            'fk-request_file-request_id',
            'request_file',
            'request_id',
            'request',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-request_file-request_id',
            'request_file'
        );

        $this->dropIndex(
            'idx-request_file-request_id',
            'request_file'
        );

        $this->dropTable('request_file');
    }
}
