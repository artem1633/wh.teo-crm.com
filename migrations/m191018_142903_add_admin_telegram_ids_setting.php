<?php

use yii\db\Migration;

/**
 * Class m191018_142903_add_admin_telegram_ids_setting
 */
class m191018_142903_add_admin_telegram_ids_setting extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->insert('settings', [
            'key' => 'admin_telegram_ids',
            'label' => 'Your telegram admin ids comma separated',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
    }
}
