<?php

use yii\db\Migration;

/**
 * Handles adding telegram_id to table `candidate`.
 */
class m191018_132830_add_telegram_id_column_to_candidate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('candidate', 'telegram_id', $this->string());
        $this->addColumn('candidate', 'time_zone', $this->integer());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('candidate', 'telegram_id');
        $this->dropColumn('candidate', 'time_zone');
    }
}
