<?php

use yii\db\Migration;

/**
 * Handles adding candidate_id to table `faq`.
 */
class m201122_123954_add_candidate_id_column_to_faq_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('faq', 'candidate_id', $this->integer()->comment('Кандидат'));

        $this->createIndex(
            'idx-faq-candidate_id',
            'faq',
            'candidate_id'
        );

        $this->addForeignKey(
            'fk-faq-candidate_id',
            'faq',
            'candidate_id',
            'user',
            'id',
            'SET NULL'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey(
            'fk-faq-candidate_id',
            'faq'
        );

        $this->dropIndex(
            'idx-faq-candidate_id',
            'faq'
        );

        $this->dropColumn('faq', 'candidate_id');
    }
}
