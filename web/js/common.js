$('#btn-dropdown_header').click(function(){
    if($(".dropdown-menu").is(':visible')){
        $(".dropdown-menu ").hide();
    } else {
        $(".dropdown-menu ").show();
    }
});

function calculate(input)
{
    var val = input.val();
    var usd = input.data('current-price');

    $('#calculate-value').text(Number(val / usd).toFixed(8));
}