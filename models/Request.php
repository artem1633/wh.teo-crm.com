<?php

namespace app\models;

use app\modules\api\controllers\V1Controller;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "request".
 *
 * @property int $id
 * @property int $client_id Клиент
 * @property int $status
 * @property string $zip
 * @property string $state
 * @property string $bank
 * @property double $sum
 * @property string $status_changed_at
 * @property string $created_at
 * @property int $candidate_id
 * @property string $recs
 * @property string $recs_payment
 * @property string $done_datetime
 * @property integer $continued
 * @property integer $is_exists
 * @property double $sum_payment
 * @property string $receive_date
 * @property double $done_sum
 * @property int $candidate_visible_id Кандидат
 *
 * @property Candidate $candidate
 * @property Candidate $candidateVisible
 * @property User $client
 */
class Request extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 0;
    const STATUS_DOCS_LOADED = 1;
    const STATUS_DONE = 2;
    const STATUS_CLOSED = 3;
    const STATUS_FAILED = 4;
    const STATUS_PAYMENT = 5;
    const STATUS_WAITING = 6;
    const STATUS_WORK = 7;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'request';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => null,
                'createdByAttribute' => 'client_id',
                'value' => Yii::$app->user->getId(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['client_id', 'candidate_id', 'candidate_visible_id'], 'integer'],
            [['sum', 'sum_payment'], 'number'],
            [['recs', 'recs_payment', 'receive_date'], 'string'],
            [['status_changed_at', 'created_at', 'done_datetime', 'continued', 'is_exists'], 'safe'],
            [['zip', 'state', 'bank'], 'string', 'max' => 255],
            [['candidate_visible_id'], 'exist', 'skipOnError' => true, 'targetClass' => Candidate::className(), 'targetAttribute' => ['candidate_visible_id' => 'id']],
            [
                ['candidate_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Candidate::className(),
                'targetAttribute' => ['candidate_id' => 'id']
            ],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'status' => 'Status',
            'zip' => 'Zip',
            'state' => 'State',
            'bank' => 'Bank',
            'sum' => 'Sum',
            'status_changed_at' => 'Status Changed At',
            'candidate_id' => 'Candidate ID',
            'receive_date' => 'Receive Date',
            'recs' => 'Recs',
            'done_sum' => 'Amount',
            'done_datetime' => 'Done date and time',
            'candidate_visible_id' => 'Candidate',
            'created_at' => 'Created At',
        ];
    }

    public function nextVisibleCandidate()
    {
        if($this->candidate_visible_id){
            $currentCandidate = Candidate::findOne($this->candidate_visible_id);
            $currentUser = User::findOne($currentCandidate->user_id);

            $nextUser = User::find()
                ->where(['role' => User::ROLE_CANDIDATE])
                ->andWhere(['>', 'candidate_visible_sort', $currentUser->candidate_visible_sort])
                ->orderBy('candidate_visible_sort asc')
                ->one();

            $nextCandidate = Candidate::find()->where(['user_id' => $nextUser->id])->one();

            if($nextCandidate){
                $this->candidate_visible_id = $nextCandidate->id;
                $this->save(false);
            } else {
                $this->candidate_visible_id = null;
                $this->save(false);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord) {
            $this->status = self::STATUS_NEW;
            $bank = str_replace(' ', '+', $this->bank);
            $html = file_get_contents("https://www.usbanklocations.com/banks.php?q={$bank}&lc={$this->zip}");

            $response = strip_tags($html);
            if (stripos($response, 'We can not') === false) {
                $this->is_exists = 1;
            } else {
                $this->is_exists = 0;
            }

            $firstUser = User::find()->where(['role' => User::ROLE_CANDIDATE])->orderBy('candidate_visible_sort asc')->one();
            if($firstUser){
                $firstCandidate = Candidate::find()->andWhere(['user_id' => $firstUser->id])->one();
                if($firstCandidate){
                    $this->candidate_visible_id = $firstCandidate->id;
                }
            }
        }

//        $sumPer = $this->sum / 100 * 15;
//        $checkSum = $this->sum - $sumPer;

        if ($this->sum < 2000) {
            Yii::info('Сумма меньше 2000', 'test');

            $this->sum_payment = $this->sum - ($this->sum / 100 * 20);
        } elseif ($this->sum >= 2000) {
            Yii::info('Сумма больше 1999', 'test');

            $sumPer = $this->sum / 100 * 15;
            $checkSum = $this->sum - $sumPer;
            $this->sum_payment = $checkSum;
        }

        return parent::beforeSave($insert);
    }

    public function getTimer()
    {
        if ($this->done_datetime == null) {
            return '—';
        }

        $now = new \DateTime();
        $end = date_create(date('Y-m-d H:i:s', strtotime($this->done_datetime))); // 10800 — 3 hours
        $interval = date_diff($now, $end);
        $diff = $interval->format('%H:%I');

        if ($end->getTimestamp() < time() && $this->continued == false) {
            return Html::a('Continue', ['request/continue', 'id' => $this->id], [
                'class' => 'btn btn-primary btn-sm',
                'role' => 'modal-remote',
                'title' => 'Continue',
                'data-confirm' => false,
                'data-method' => false,// for overide yii data api
                'data-request-method' => 'post',
                'data-confirm-title' => 'Confirm',
                'data-confirm-message' => 'Continue that request?'
            ]);
        }

        if ($this->continued == true && $end->getTimestamp() < time()) {
            return Html::tag('span', 'Time Expired', ['class' => 'label label-danger', 'style' => 'font-size: 12px;']);
        }

        return $diff;
    }

    /**
     * @return array
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_NEW => 'New',
            self::STATUS_DOCS_LOADED => 'Documents loaded',
            self::STATUS_DONE => 'Done',
            self::STATUS_FAILED => 'Failed',
            self::STATUS_CLOSED => 'Closed',
            self::STATUS_WAITING => 'Waiting',
            self::STATUS_WORK => 'In Work',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (in_array('status', array_keys($changedAttributes))) {
            if ($changedAttributes['status'] != $this->status) {
                $this->status_changed_at = date('Y-m-d H:i:s');
                $this->save(false);
            }
        }

        if ($insert) {
            $html = "New request created\n";
            foreach ($changedAttributes as $key => $value) {
                $html .= "{$this->getAttributeLabel($key)}: {$this->$key}\n";
            }
            $adminIds = explode(',', Settings::findByKey('admin_telegram_ids')->value);
            foreach ($adminIds as $id) {
                V1Controller::sendTelMessage($id, $html);
            }
        } else {
//            $html = "Request changed\n";
//            foreach ($changedAttributes as $key => $value)
//            {
//                $html .= "{$this->getAttributeLabel($key)}: Last value — {$value} New value — {$this->$key}\n";
//            }
//            $adminIds = explode(',', Settings::findByKey('admin_telegram_ids')->value);
//            foreach ($adminIds as $id) {
//                V1Controller::sendTelMessage($id, $html);
//            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidate()
    {
        return $this->hasOne(Candidate::className(), ['id' => 'candidate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateVisible()
    {
        return $this->hasOne(Candidate::className(), ['id' => 'candidate_visible_id']);
    }

    /**
     * @return mixed|null
     */
    public function getStateName()
    {
        /** @var State $model */
        $model = State::find()->where(['alias' => $this->state])->one();

        if ($model) {
            return $model->name;
        }

        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(User::className(), ['id' => 'client_id']);
    }
}
