<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Request;
use yii\helpers\ArrayHelper;

/**
 * RequestSearch represents the model behind the search form about `app\models\Request`.
 */
class RequestSearch extends Request
{
    public $showHidden;

    public $statuses;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'client_id', 'status'], 'integer'],
            [['zip', 'state', 'bank', 'status_changed_at', 'created_at', 'showHidden', 'statuses'], 'safe'],
            [['sum'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Request::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC,
                ],
            ],
        ]);

        $statuses = $this->statuses;

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'client_id' => $this->client_id,
            'status' => $this->status,
            'sum' => $this->sum,
            'status_changed_at' => $this->status_changed_at,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'zip', $this->zip])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'bank', $this->bank]);


        if($statuses == null){
            if($this->showHidden == 1){
                $query->andWhere(['status' => [Request::STATUS_CLOSED, Request::STATUS_FAILED]]);
            } else {
                $query->andWhere(['not in', 'status', [Request::STATUS_CLOSED, Request::STATUS_FAILED]]);
            }
        }

        if(Yii::$app->user->identity->isCandidate()){
            $candidatePks = ArrayHelper::getColumn(Candidate::find()->where(['user_id' => Yii::$app->user->getId()])->all(), 'id');
            $query->andWhere(['or', ['status' => Request::STATUS_NEW], ['candidate_id' => $candidatePks]]);
            $query->andWhere(['candidate_visible_id' => $candidatePks]);
        }

        if($statuses != null){
            $query->andWhere(['status' => $statuses]);
        }


        return $dataProvider;
    }
}
