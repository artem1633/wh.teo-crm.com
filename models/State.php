<?php

namespace app\models;

use yii2tech\filedb\ActiveRecord;

/**
 * Class State
 * @package app\models
 *
 * @property string $alias
 * @property string $name
 */
class State extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function fileName()
    {
        return 'State';
    }
}