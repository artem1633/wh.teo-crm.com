<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "candidate".
 *
 * @property int $id
 * @property string $state Штат
 * @property string $bank Банк
 * @property string $account Счет
 * @property string $checking_number
 * @property string $routing_number
 * @property string $address
 * @property integer $status
 * @property string $telegram_id
 * @property integer $time_zone
 * @property int $user_id
 *
 * @property string $stateName
 * @property User[] $users
 */
class Candidate extends \yii\db\ActiveRecord
{
    const STATUS_READY = 0;
    const STATUS_AWAIT = 1;
    const STATUS_NOT_READY = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'candidate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['state', 'bank', 'account', 'checking_number'], 'required'],
            [['state', 'account', 'checking_number', 'routing_number', 'address', 'telegram_id'], 'string', 'max' => 255],
            [['checked', 'status', 'time_zone'], 'integer'],
            [['bank'], 'safe'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'state' => 'State',
            'telegram_id' => 'Telegram account',
            'bank' => 'Bank name',
            'account' => 'Account name',
            'time_zone' => 'Time Zone',
            'checking_number' => 'Checking Number',
            'routing_number' => 'Routing Number',
            'address' => 'Address',
            'checked' => 'Checked',
            'user_id' => 'User',
        ];
    }

    /**
     * @return array
     */
    public static function statusLabels()
    {
        return [
            self::STATUS_READY => 'Ready to work',
            self::STATUS_AWAIT => 'Waiting',
            self::STATUS_NOT_READY => 'Does not work',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if($this->bank != null){
            if(is_array($this->bank)){
                $this->bank = implode(',', $this->bank);
            }
        }

        if(Yii::$app->user->identity->isCandidate()){
            $this->user_id = Yii::$app->user->getId();
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return mixed|null
     */
    public function getStateName()
    {
        $model = State::find()->where(['alias' => $this->state])->one();

        if($model){
            return $model->name;
        }

        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCandidateFiles()
    {
        return $this->hasMany(CandidateFile::className(), ['candidate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFaqs()
    {
        return $this->hasMany(Faq::className(), ['candidate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequests()
    {
        return $this->hasMany(Request::className(), ['candidate_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['candidate_id' => 'id']);
    }
}
