<?php

namespace app\models\forms;

use app\models\Request;
use app\models\User;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\db\Query;
use yii\helpers\VarDumper;

/**
 * Class ReportForm
 * @package app\models\forms
 */
class ReportForm extends Model
{
    /**
     * @var string
     */
    public $dates;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dates'], 'safe']
        ];
    }

    public function searchClient($params)
    {
        $this->load($params);

        $dates = explode(' - ', $this->dates);

        if(isset($dates[1]) == false){
            $dates[0] = null;
            $dates[1] = null;
        }

        $statusDone = Request::STATUS_DONE;
        $statusFailed = Request::STATUS_FAILED;
        $statusClosed = Request::STATUS_CLOSED;

        $requests = (new Query())
            ->select("user.id as user_id, user.name as user_name, count(request.id) as request_count,
             count(case request.status when {$statusDone} then 1 else null end) as request_done_count,
             count(case request.status when {$statusFailed} then 1 else null end) as request_failed_count,
             sum(case when request.status = {$statusClosed} then request.sum else 0 end) as request_total_sum")
            ->from('user')
            ->leftJoin('request', 'request.client_id=user.id')
            ->andWhere(['user.role' => User::ROLE_USER, 'request.status' => [Request::STATUS_CLOSED, Request::STATUS_FAILED]])
            ->andFilterWhere(['between', 'request.created_at', $dates[0], $dates[1]])
            ->groupBy('user.id')
            ->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $requests
        ]);

        return $dataProvider;
    }

    public function searchCandidate($params)
    {
        $this->load($params);

        $dates = explode(' - ', $this->dates);

        if(isset($dates[1]) == false){
            $dates[0] = null;
            $dates[1] = null;
        }

        $statusFailed = Request::STATUS_FAILED;
        $statusClosed = Request::STATUS_CLOSED;

        $requests = (new Query())
            ->select("candidate.id as candidate_id, candidate.account as candidate_account,
            count(case request.status when {$statusClosed} then 1 else null end) as request_count,
            sum(case when request.status = {$statusClosed} then request.sum else 0 end) as request_total_sum")
            ->from('candidate')
            ->leftJoin('request', 'request.candidate_id=candidate.id')
            ->andWhere(['request.status' => [Request::STATUS_CLOSED, Request::STATUS_FAILED]])
            ->andFilterWhere(['between', 'request.created_at', $dates[0], $dates[1]])
            ->groupBy('candidate.id')
            ->all();

        $dataProvider = new ArrayDataProvider([
            'allModels' => $requests
        ]);

        return $dataProvider;
    }
}