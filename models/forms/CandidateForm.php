<?php

namespace app\models\forms;

use app\models\Candidate;
use app\models\Request;
use app\models\Settings;
use app\modules\api\controllers\V1Controller;
use PHPHtmlParser\Dom;
use yii\base\Model;

/**
 * Class CandidateForm
 * @package app\models\forms
 */
class CandidateForm extends Model
{
    /**
     * @var integer
     */
    public $requestId;

    /**
     * @var integer
     */
    public $candidateId;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['requestId', 'candidateId'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function apply()
    {
        $request = Request::findOne($this->requestId);
        $request->candidate_id = $this->candidateId;
        $candidate = Candidate::findOne($this->candidateId);
        $timeSetting = Settings::findByKey('first_timer');
        if($timeSetting == null){
            $timeSetting = 0;
        } else {
            $timeSetting = $timeSetting->value * 60;
        }


        $now = new \DateTime();
        $end = date_create(date('Y-m-d H:i:s', $now->getTimestamp() + $timeSetting)); // 10800 — 3 hours
        $request->done_datetime = $end->format('Y-m-d H:i:s');

        $request->recs = "1. Head to {$candidate->bank}  branch. Bring your ID.<br>
                2. Go to the teller who will make a Cash deposit to the Business Account. <br>
                3. Complete a cash deposit to:<br>
                Account name: {$candidate->account}<br>
                Bank name: {$candidate->bank}<br>
                Checking number:  {$candidate->checking_number}<br>
                Routing number: {$candidate->routing_number}<br>
                Address: {$candidate->address}<br>
                4. Email me a copy of the receipt";

        if($request->is_exists == 1){
            $bank = str_replace(' ', '+', $request->bank);
            $html = file_get_contents("https://www.usbanklocations.com/banks.php?q={$bank}&lc={$request->zip}");
            $dom = new Dom();
            $dom->load($html);

            $plb = $dom->find('.plb');

            if(count($plb) > 0){
                $el = $plb[0];
                $el->find('.txb')->delete();
                $el->find('b')->delete();
                $html = $el->innerHtml;
                $html = str_replace('<br />', " ", $html);
                $parts = explode( ') ', $html);
                if(count($parts) == 2){
                    $html = $parts[1];
                }

                $request->recs .= "<br> Address branch: {$html}";
            }
        }

        $candidate->save(false);

//        $adminIds = explode(',', Settings::findByKey('admin_telegram_ids')->value);
//        $html = "Candidate with id {$candidate->id} assigned to request with id {$request->id}";
//        foreach ($adminIds as $id) {
//            V1Controller::sendTelMessage($id, $html);
//        }


        if($candidate->telegram_id != null)
        {
            $setting = Settings::findByKey('second_message')->value;
            V1Controller::sendTelMessage($candidate->telegram_id, $setting);
        }

        $request->status = Request::STATUS_WAITING;

        return $request->save(false);
    }
}