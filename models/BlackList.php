<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "black_list".
 *
 * @property int $id
 * @property string $name
 * @property string $state
 * @property int $user_id Создатель записи в таблице
 * @property string $created_at
 * @property string $user_info Информация об авторе
 *
 * @property User author
 */
class BlackList extends ActiveRecord
{
    public $user_info;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'black_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'state', 'user_info'], 'string'],
            ['name', 'required'],
            [['user_id'], 'integer'],
            [['created_at'], 'safe'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'state' => 'State',
            'user_id' => 'Author',
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert){
            $this->created_at = date('Y-m-d H:i:s', time());
            $this->user_id = Yii::$app->user->id;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' =>'user_id']);
    }

    /**
     * Получает и возвращает логин и имя автора записи
     * @return string
     */
    public function getAuthorInfo()
    {
        $login = $this->author->login ?? null;
        $name = $this->author->name ?? null ? 'Name: ' . $this->author->name : '';
        return 'Login: ' . $login . '<br>' . $name;
    }

    public function isRecordAuthor()
    {
        if (Yii::$app->user->id === $this->user_id || Yii::$app->user->identity->isSuperAdmin()){
            return true;
        }

        return false;
    }
}
