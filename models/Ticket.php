<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ticket".
 *
 * @property int $id
 * @property int $user_id Пользователь
 * @property string $subject Заголовок
 * @property string $description Описание
 * @property string $status Статус
 * @property int $is_read Прочитано
 * @property string $last_message_datetime Дата и время последнего сообщения
 * @property string $created_at Дата и время создания
 * @property string $closed_datetime Дата и время закрытия
 *
 * @property User $user
 * @property TicketMessage[] $ticketMessages
 */
class Ticket extends \yii\db\ActiveRecord
{
    const READ_ADMIN = 0;
    const READ_USER = 1;

    const STATUS_NEW = 0;
    const STATUS_WORK = 1;
    const STATUS_DONE = 2;
    const STATUS_REJECTED = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ticket';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject', 'description'], 'required'],
            [['user_id', 'is_read'], 'integer'],
            [['description'], 'string'],
            [['last_message_datetime', 'created_at', 'closed_datetime'], 'safe'],
            [['subject', 'status'], 'string', 'max' => 255],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User',
            'subject' => 'Title',
            'description' => 'Problem description',
            'status' => 'Status',
            'is_read' => 'Is read',
            'last_message_datetime' => 'Last message date and time',
            'created_at' => 'Created at',
            'closed_datetime' => 'Done Date and time',
        ];
    }

    /**
     *
     */
    public function hasUnread()
    {
        if(in_array($this->status, [self::STATUS_DONE, self::STATUS_REJECTED])) {
            return false;
        }

        if(Yii::$app->user->identity->isSuperAdmin()){
            return $this->is_read !== self::READ_USER;
        } else {
            return $this->is_read !== self::READ_ADMIN;
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if(Yii::$app->user->identity->isSuperAdmin() == false){
            $this->user_id = Yii::$app->user->identity->id;
        }

        if($this->isNewRecord) {
            $this->status = self::STATUS_NEW;
            $this->is_read = 0;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTicketMessages()
    {
        return $this->hasMany(TicketMessage::className(), ['ticket_id' => 'id']);
    }

    /**
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::STATUS_NEW => 'New',
            self::STATUS_WORK => 'Work',
            self::STATUS_DONE => 'Done',
            self::STATUS_REJECTED => 'Rejected',
        ];
    }
}
