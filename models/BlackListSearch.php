<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * BlackListSearch represents the model behind the search form about `app\models\BlackList`.
 */
class BlackListSearch extends BlackList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id'], 'integer'],
            [['name', 'state', 'user_info'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BlackList::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['user_info'] = [
            'asc' => [User::tableName() . '.login' => SORT_ASC],
            'desc' => [User::tableName() . '.login' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);
        $query->andFilterWhere(['like', 'state', $this->state]);

        $query
            ->joinWith(['author'])
            ->andFilterWhere([
                'OR',
                ['LIKE', 'user.name', $this->user_info],
                ['LIKE', 'user.login', $this->user_info],
            ]);


        return $dataProvider;
    }
}
